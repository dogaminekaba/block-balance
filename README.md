# Description

Take a block from Block Box and start building your tower. If it collapses, game over. Try to use as many blocks as you can! 

For graphics Starling Engine is used, for physical calculations Nape Physics Engine is used.

# Screenshots

![BlockBalance_start](https://bitbucket.org/dogaminekaba/block-balance/downloads/BlockBalance_start.png)

![BlockBalance_play](https://bitbucket.org/dogaminekaba/block-balance/downloads/BlockBalance_play.png)

![BlockBalance_end](https://bitbucket.org/dogaminekaba/block-balance/downloads/BlockBalance_end.png)