package objects
{
  import nape.phys.Body;
  import nape.phys.BodyType;
  import nape.shape.Polygon;
  
  import starling.display.MovieClip;

  public class RectangleBlock extends Block
  {
    private var rectangleWidth:Number;
    private var rectangleHeight:Number;
    public function RectangleBlock(width:Number, height:Number)
    {
      super();
      rectangleWidth = width;
      rectangleHeight = height;
      
      blockArt = new MovieClip(Assets.getAtlas().getTextures("rectangle_image"));
      blockBody = new Body(BodyType.DYNAMIC);
      blockBody.shapes.add(new Polygon(Polygon.box(rectangleWidth, rectangleHeight)));
      
      blockArt.pivotX = blockArt.width/2;
      blockArt.pivotY = blockArt.height/2;
      
      blockArt.scaleX = rectangleWidth/blockArt.width;
      blockArt.scaleY = rectangleHeight/blockArt.height;
      
      blockBody.userData.graphic = blockArt;
      addChild(blockBody.userData.graphic);
    }
  }
}