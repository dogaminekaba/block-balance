package objects
{
  import nape.geom.Vec2;
  import nape.geom.Vec2List;
  import nape.phys.Body;
  import nape.phys.BodyType;
  import nape.shape.Polygon;
  
  import starling.display.MovieClip;

  public class TrapezoidBlock extends Block
  {
    private var trapezoidWidth:Number;
    private var trapezoidHeight:Number;
    public function TrapezoidBlock(width:Number)
    {
      super();
      trapezoidWidth = width;
      trapezoidHeight = trapezoidWidth * 0.75;
      
      blockArt = new MovieClip(Assets.getAtlas().getTextures("trapezoid_image"));
      blockBody = new Body(BodyType.DYNAMIC);
      
      var pointList:Vec2List = new Vec2List();
      pointList.add(new Vec2(0,0));
      pointList.add(new Vec2(trapezoidWidth,0));
      pointList.add(new Vec2(trapezoidWidth + 10, trapezoidHeight));
      pointList.add(new Vec2(-10, trapezoidHeight));
      
      blockBody.shapes.add(new Polygon(pointList));
      blockBody.align();
      
      blockArt.pivotX = blockArt.width/2;
      blockArt.pivotY = blockArt.height/2;
      
      blockArt.scaleX = trapezoidWidth/(blockArt.width/2);
      blockArt.scaleY = trapezoidHeight/blockArt.height;
      
      blockBody.userData.graphic = blockArt;
      addChild(blockBody.userData.graphic);
    }
  }
}