package objects
{
  
  import nape.phys.Body;
  
  import starling.display.MovieClip;
  import starling.display.Sprite;
  
  public class Block extends Sprite
  {
    public var blockBody:Body;
    protected var blockArt:MovieClip;
    
    public function Block()
    {
      super();
    }
  }
}