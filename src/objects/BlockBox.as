package objects
{
  
  import starling.display.Image;
  import starling.display.Sprite;
  
  import utilities.GameProperties;
  
  public class BlockBox extends Sprite
  {
    public var frontImage:Image;
    private var backImage:Image;
    
    public function BlockBox()
    {
      super();
      
      frontImage = new Image(Assets.getAtlas().getTexture("blockBox_front_image"));
      frontImage.x = GameProperties.blockBoxPositionX;
      frontImage.y = GameProperties.blockBoxPositionY;
      backImage = new Image(Assets.getAtlas().getTexture("blockBox_back_image"));
      backImage.x = frontImage.x;
      backImage.y = frontImage.y - 12;
      
      addChild(frontImage);
      addChild(backImage);
    }
  }
}