package objects
{
  import nape.phys.Body;
  import nape.phys.BodyType;
  import nape.shape.Polygon;
  
  import starling.display.MovieClip;

  public class TriangleBlock extends Block
  {
    private var triangleRadius:Number;
    public function TriangleBlock(radius:Number)
    {
      super();
      
      triangleRadius = radius;
      
      blockArt = new MovieClip(Assets.getAtlas().getTextures("triangle_image"));
      blockBody = new Body(BodyType.DYNAMIC);
      blockBody.shapes.add(new Polygon(Polygon.regular(triangleRadius, triangleRadius, 3, Math.PI*3/2)));
      
      blockArt.pivotX = blockArt.width/2;
      blockArt.pivotY = blockArt.height * (2.0/3);
      
      blockArt.scaleX = triangleRadius/(blockArt.width*2/3);
      blockArt.scaleY = triangleRadius/(blockArt.height*2/3);
      
      blockBody.userData.graphic = blockArt;
      addChild(blockBody.userData.graphic);
    }
  }
}