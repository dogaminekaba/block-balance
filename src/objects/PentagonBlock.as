package objects
{
  import nape.phys.Body;
  import nape.phys.BodyType;
  import nape.shape.Polygon;
  
  import starling.display.MovieClip;

  public class PentagonBlock extends Block
  {
    private var pentagonRadius:Number;
    public function PentagonBlock(radius:Number)
    {
      super();
      
      pentagonRadius = radius;
      
      blockArt = new MovieClip(Assets.getAtlas().getTextures("pentagon_image"));
      blockBody = new Body(BodyType.DYNAMIC);
      blockBody.shapes.add(new Polygon(Polygon.regular(pentagonRadius, pentagonRadius, 5, Math.PI*3.0/2)));
      
      blockArt.pivotX = blockArt.width/2;
      blockArt.pivotY = blockArt.height/2;
      
      blockArt.scaleX = pentagonRadius/(blockArt.width/2);
      blockArt.scaleY = pentagonRadius/(blockArt.height/2);
      
      blockBody.userData.graphic = blockArt;
      addChild(blockBody.userData.graphic);
    }
  }
}