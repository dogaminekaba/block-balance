package objects
{
  import nape.phys.Body;
  import nape.phys.BodyType;
  import nape.shape.Polygon;
  
  import starling.display.MovieClip;

  public class HexagonBlock extends Block
  {
    private var hexagonRadius:Number;
    public function HexagonBlock(radius:Number)
    {
      super();
      
      hexagonRadius = radius;
      
      blockArt = new MovieClip(Assets.getAtlas().getTextures("hexagon_image"));
      blockBody = new Body(BodyType.DYNAMIC);
      blockBody.shapes.add(new Polygon(Polygon.regular(hexagonRadius, hexagonRadius, 6)));
      
      blockArt.pivotX = blockArt.width/2;
      blockArt.pivotY = blockArt.height/2;
      
      blockArt.scaleX = hexagonRadius/(blockArt.width/2);
      blockArt.scaleY = hexagonRadius/(blockArt.height/2);
      
      blockBody.userData.graphic = blockArt;
      addChild(blockBody.userData.graphic);
    }
  }
}