package objects
{
  import nape.phys.Body;
  import nape.phys.BodyType;
  import nape.shape.Polygon;
  
  import starling.display.MovieClip;

  public class SquareBlock extends Block
  {
    private var squareLength:Number;
    public function SquareBlock(length:Number)
    {
      super();
      
      squareLength = length;
      
      blockArt = new MovieClip(Assets.getAtlas().getTextures("square_image"));
      blockBody = new Body(BodyType.DYNAMIC);
      blockBody.shapes.add(new Polygon(Polygon.box(squareLength, squareLength)));
      
      blockArt.pivotX = blockArt.width/2;
      blockArt.pivotY = blockArt.height/2;

      blockArt.scaleX = squareLength/blockArt.width;
      blockArt.scaleY = squareLength/blockArt.height;
      
      blockBody.userData.graphic = blockArt;
      addChild(blockBody.userData.graphic);
    }
  }
}