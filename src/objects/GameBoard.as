package objects
{
  import nape.phys.Body;
  import nape.phys.BodyType;
  import nape.shape.Polygon;
  import nape.space.Space;
  
  import starling.display.Image;
  import starling.display.Sprite;
  
  import utilities.GameProperties;
  
  public class GameBoard extends Sprite
  {
    private var boardImage:Image;
    private var boardBody:Body;
    public function GameBoard(space:Space)
    {
      super();
      
      boardImage = new Image(Assets.getAtlas().getTexture("board_image"));
      boardBody = new Body(BodyType.STATIC);
      
      boardBody.shapes.add(new Polygon(Polygon.box(GameProperties.boardWidth, GameProperties.boardHeight)));
      boardBody.position.setxy(GameProperties.boardPositionX , GameProperties.boardPositionY);
      
      boardImage.x = boardBody.position.x - boardImage.width/2;
      boardImage.y = boardBody.position.y - boardImage.height/2;
      
      boardBody.space = space;
      addChild(boardImage);
    }
  }
}