package utilities
{
  import objects.Block;
  import objects.HexagonBlock;
  import objects.PentagonBlock;
  import objects.RectangleBlock;
  import objects.SquareBlock;
  import objects.TrapezoidBlock;
  import objects.TriangleBlock;

  public class GameUtilities
  {
    
    public static function random(minNum:int, maxNum:int):int 
    {
      return (Math.floor(Math.random() * (maxNum - minNum + 1)) + minNum);
    }
    
    public static function generateBlock(data:Object):Block
    {
      switch(data.type)
      {
        case 0:
          return new TriangleBlock(data.radius);
          break;
        case 1:
          return new SquareBlock(data.length);
          break;
        case 2:
          return new RectangleBlock(data.width, data.height);
          break;
        case 3:
          return new TrapezoidBlock(data.width);
          break;
        case 4:
          return new PentagonBlock(data.radius);
          break;
        case 5:
          return new HexagonBlock(data.radius);
          break;
        default:
          return null;
          break;
      }
    }
  }
}