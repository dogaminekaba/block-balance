package utilities
{
  public class GameProperties
  {
    public static const rotateMode:Boolean = false;
    
    public static const blockTypeCount:int = 6;
    public static const stageWidth:Number = 800;
    public static const stageHeight:Number = 600;
    public static const boardPositionX:Number = (stageWidth-5)/2;
    public static const boardPositionY:Number = stageHeight - 45;
    public static const boardWidth:Number = 120;
    public static const boardHeight:Number = 30;
    public static const blockBoxPositionX:Number = 620;
    public static const blockBoxPositionY:Number = 500;
    public static const floorPositionX:Number = 0;
    public static const floorPositionY:Number = stageHeight-30;
    public static const floorWidth:Number = stageWidth;
    public static const floorHeight:Number = 100;
    public static const borderThickness:Number = 5;
  }
}