package screens
{
  import flash.events.MouseEvent;
  
  import starling.core.Starling;
  import starling.display.Button;
  import starling.display.MovieClip;
  import starling.display.Sprite;
  import starling.events.Event;
  
  public class Welcome extends Sprite
  {
    private var bg:MovieClip;
    private var playBtn:Button;
    private var infoBtn:Button;
    private static var _starling:Starling;
    
    public function Welcome()
    {
      super();
      this.addEventListener(starling.events.Event.ADDED_TO_STAGE, onAddedToStage);
    }
    
    private function onAddedToStage():void
    {
      trace("welcome screen initialized");
      drawScreen();
    }
    
    private function drawScreen():void
    {
      bg = new MovieClip(Assets.getAtlas().getTextures("welcome"));
      this.addChild(bg);
      
      playBtn = new Button(Assets.getAtlas().getTexture("play_button"));
      playBtn.x = 260;
      playBtn.y = 280;
      this.addChild(playBtn);
      
      infoBtn = new Button(Assets.getAtlas().getTexture("info_button"));
      infoBtn.x = 305;
      infoBtn.y = 400;
      this.addChild(infoBtn);
      
    }
    public function initialize():void
    {
      this.visible = true;
    }
    
  }
}