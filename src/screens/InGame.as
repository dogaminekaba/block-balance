package screens
{
  import nape.callbacks.CbEvent;
  import nape.callbacks.CbType;
  import nape.callbacks.InteractionCallback;
  import nape.callbacks.InteractionListener;
  import nape.callbacks.InteractionType;
  import nape.geom.Vec2;
  import nape.phys.Body;
  import nape.phys.BodyType;
  import nape.shape.Polygon;
  import nape.space.Space;
  
  import objects.Block;
  import objects.BlockBox;
  import objects.GameBoard;
  
  import starling.display.Image;
  import starling.display.Sprite;
  import starling.events.Event;
  import starling.events.Touch;
  import starling.events.TouchEvent;
  import starling.events.TouchPhase;
  
  import utilities.GameProperties;
  import utilities.GameUtilities;
  
  public class InGame extends Sprite
  {
    private var space:Space;
    private var block:Block;
    private var blockBox:BlockBox;
    private var bg:Image;
    private var insideBox:Boolean;
    private var interactionListener:InteractionListener;
    private var floorCollisionType:CbType = new CbType();
    private var blockCollisionType:CbType = new CbType();
    
    public function InGame()
    {
      super();
      
      if (stage != null) 
      {
        init(null);
      }
      else
      {
        addEventListener(starling.events.Event.ADDED_TO_STAGE, init);
      }
      
      var triangleBlock:Object = {"type":0, "radius":20};
      var squareBlock:Object = {"type":1, "length":20};
      var rectangleBlock:Object = {"type":2, "width":40, "height":20};
      var trapezoidBlock:Object = {"type":3, "width":20};
      var pentagonBlock:Object = {"type":4, "radius":20};
      var hexagonBlock:Object = {"type":5, "radius":20};
      
    }
    
    private function init(event:Event):void
    {
      if(event != null)
      {
        removeEventListener(Event.ADDED_TO_STAGE, init);
      }
      
      /*Building space*/
      space = new Space(new Vec2(0,600));
      
      setUp();
    }
    
    private function setUp():void
    {
      interactionListener = new InteractionListener(CbEvent.BEGIN, InteractionType.COLLISION, floorCollisionType, blockCollisionType, blockToFloor);        
      space.listeners.add(interactionListener);
      
      /*Add background*/
      addChild(new Image(Assets.getAtlas().getTexture("background")));
      
      /*Create BlockBox*/
      blockBox = new BlockBox();
      addChild(blockBox);
      addChild(blockBox.frontImage);
      
      blockBox.frontImage.addEventListener(TouchEvent.TOUCH, onTouch);
      
      /*Create floor*/
      var floor:Body = new Body(BodyType.STATIC);
      floor.shapes.add(new Polygon(Polygon.rect(GameProperties.floorPositionX, GameProperties.floorPositionY, GameProperties.floorWidth, GameProperties.floorHeight)));
      floor.cbTypes.add(floorCollisionType);
      floor.space = space;
      
      /*Create frame Borders*/
      var borders:Body = new Body(BodyType.STATIC);
      borders.shapes.add(new Polygon(Polygon.rect(0, 0, stage.width, GameProperties.borderThickness)));
      borders.shapes.add(new Polygon(Polygon.rect(0, 0, GameProperties.borderThickness, stage.height)));
      borders.shapes.add(new Polygon(Polygon.rect(stage.width-GameProperties.borderThickness, 0, GameProperties.borderThickness, stage.height)));
      borders.space = space;
      
      /*Create game board*/
      var gameBoard:GameBoard = new GameBoard(space);
      addChild(gameBoard);
      
      addEventListener(Event.ENTER_FRAME, onEnterFrame);
    }
    
    private function blockToFloor(collision:InteractionCallback):void
    {
      space.liveBodies.foreach(stopBlocks);
    }
    
    private function stopBlocks(b:Body):void
    {
      b.type = BodyType.STATIC;
      blockBox.frontImage.removeEventListener(TouchEvent.TOUCH, onTouch);
    }
    
    private function onEnterFrame(e:Event):void 
    {
      space.step(1 / 60);
      space.liveBodies.foreach(updateGraphics);
    }
    
    private function updateGraphics(b:Body):void
    {
      b.userData.graphic.x = b.position.x;
      b.userData.graphic.y = b.position.y;
      b.userData.graphic.rotation = b.rotation;
    }
    
    private function onResponse(data:Object):void
    {
      /*Create block*/
      insideBox = true;
      block = GameUtilities.generateBlock(data.block);
      block.x = blockBox.frontImage.x + blockBox.frontImage.width/2;
      block.y = blockBox.frontImage.y + 17;
      
      block.blockBody.cbTypes.add(blockCollisionType);
      block.blockBody.space = space;
      block.blockBody.allowMovement = false;
      block.blockBody.allowRotation = false;
      if (GameProperties.rotateMode) 
      {
        block.blockBody.rotation = GameUtilities.random(0,360);
      }
      addChild(block);
      swapChildren(blockBox.frontImage, block);
    }
    
    private function onTouch(event:TouchEvent):void
    {
      var touch:Touch = event.getTouch(stage);
      var grapicAdded:Boolean = false;
      
      if(touch)
      {
        switch(touch.phase)
        {
          case TouchPhase.BEGAN:
            connector.serviceModel.requestData("getblockdata",{},onResponse);
            break;
          case TouchPhase.ENDED:
            if(insideBox)
              removeChild(block);
            /*Drop the block*/
            else
            {
              block.blockBody.position.setxy(touch.globalX, touch.globalY);
              block.blockBody.allowMovement = true;
              block.blockBody.allowRotation = true;
            }
            break;
          case TouchPhase.MOVED:
            /*Moving out of box effect*/
            if(insideBox)
            {
              if(touch.globalY + block.height < blockBox.frontImage.y + blockBox.frontImage.height -14)
                block.y = touch.globalY;
              
              /*Moved out of the box*/
              if(block.y + block.height < blockBox.frontImage.y)
              {
                this.swapChildren(blockBox.frontImage, block);
                block.x = touch.globalX;
                block.y = touch.globalY;
                insideBox = false;
              }
            }
            /*Out of box*/
            else
            {
              block.blockBody.position.setxy(touch.globalX, touch.globalY);
              if (!grapicAdded) 
              {
                block.blockBody.userData.graphic.x = block.x;
                block.blockBody.userData.graphic.y = block.y;
                addChild(block.blockBody.userData.graphic);
                grapicAdded = true;
              }
            }
            break;
          default:
            break;
        }
      }
    }
  }
}