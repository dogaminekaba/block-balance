package
{
  import flash.display.Sprite;
  import flash.events.Event;
  import flash.events.MouseEvent;
  
  import screens.InGame;
  
  import starling.core.Starling;
  
  [SWF(frameRate="60", width="800", height="600", backgroundColor="0xddffff")]
  public class BlockBalance extends Sprite
  {
    private static var _starling:Starling;
    
    public function BlockBalance()
    {
      this.addEventListener(Event.ADDED_TO_STAGE,onAddedtoStage);
    }
    
    protected function onAddedtoStage(event:Event):void
    {
      this.removeEventListener(Event.ADDED_TO_STAGE,onAddedtoStage);
      _starling =  new Starling(InGame, stage);
      _starling.start();
      stage.addEventListener(MouseEvent.RIGHT_CLICK, onRightClick);
    }
    
    protected function onRightClick(event:MouseEvent):void
    {
      
    }
  }
}